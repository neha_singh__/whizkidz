package com.example.neha.testing;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.neha.testing.database.WatchedVideos;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public  class MainActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_REQUEST = 1;
    public static final String API_KEY = "AIzaSyDwkLj8boNJRRhNIqmpYmRXs6vseHEU9x8";

    @BindView(R.id.youtube_view) YouTubePlayerView youtubeView;
    @BindView(R.id.horizontal_recycler_view) CustomRecyclerView recyclerView;
    @BindView(R.id.fab_history) FloatingActionButton fabHistory;

    private YouTubePlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SpeedyLinearLayoutManager layoutManager
                = new SpeedyLinearLayoutManager(this, SpeedyLinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);

        // Test data
       // setupRecyclerAdapter(getTestContentList());

        RemoteAccessor accessor = new RemoteAccessor();
        accessor.ContentList(new Callback<ArrayList<ContentList>>() {

            @Override
            public void success(ArrayList<ContentList> contentLists, Response response) {
                setupRecyclerAdapter(contentLists);
            }

            @Override
           public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Unexpected Error", Toast.LENGTH_SHORT).show();
            }
        });

        youtubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youtubeView.initialize(API_KEY, this);

        fabHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, WatchedVideosActivity.class));
            }
        });
    }

    private void setupRecyclerAdapter(final ArrayList<ContentList> content) {
        VideoListAdapter adapter = new VideoListAdapter(MainActivity.this, content, new PlayVideoCallback() {

            @Override
            public void playVideo(int position) {
                if (MainActivity.this.player != null && content.size() > position) {
                    player.cueVideo(content.get(position).getVideoId());
                }

                if (content.size() > position + 1)
                    recyclerView.scrollToPosition(position + 1);

                youtubeView.setVisibility(View.VISIBLE);
                WatchedVideos.insert(content.get(position).getTitle());
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if (youtubeView.getVisibility() == View.VISIBLE)
            savedInstanceState.putInt("player_visibility", 1);
        else if (youtubeView.getVisibility() == View.INVISIBLE)
            savedInstanceState.putInt("player_visibility", 2);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        int visibility = savedInstanceState.getInt("player_visibility");
        if (visibility == 1)
            youtubeView.setVisibility(View.VISIBLE);
        else if (visibility == 2)
            youtubeView.setVisibility(View.INVISIBLE);

        super.onRestoreInstanceState(savedInstanceState);
    }

    private ArrayList<ContentList> getTestContentList() {
        ArrayList<ContentList> contentList = new ArrayList<>();

        ContentList obj1 = new ContentList();
        obj1.setTitle("Shapes - Nursery Rhyme");
        obj1.setUrl("https://www.youtube.com/watch?v=Iq4O3b5Vp1w");

        ContentList obj2 = new ContentList();
        obj2.setTitle("Learn about Shapes with Elly - Fun & Educational for Babies, Toddler, Kindergarten Kids");
        obj2.setUrl("https://www.youtube.com/watch?v=Ke2Q5aGjz2c");

        contentList.add(obj1);
        contentList.add(obj2);
        contentList.add(obj2);
        contentList.add(obj2);
        contentList.add(obj2);
        contentList.add(obj2);
        contentList.add(obj2);

        return contentList;
    }


    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        this.player = player;

        if (!wasRestored) {
            player.cueVideo("6L6XqWoS8tw"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error ="Error initializing YouTube player";
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(API_KEY, this);
        }
    }

    protected Provider getYouTubePlayerProvider() {
        return youtubeView;
    }
}


