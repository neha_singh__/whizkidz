package com.example.neha.testing;

import com.example.neha.testing.api.YoutubeMetadata;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.GET;
import retrofit.http.Query;

public interface YoutubeService {

    @GET("/v3/videos")
    void getYoutubeMetadata(@Query("id") String id,
                            @Query("key") String key,
                            @Query("part") String part,
                            Callback<YoutubeMetadata> callback);

}
