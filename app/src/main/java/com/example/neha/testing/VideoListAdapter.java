package com.example.neha.testing;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.neha.testing.api.YoutubeMetadata;
import com.google.android.youtube.player.YouTubePlayer;
import com.squareup.picasso.Picasso;
import com.thefinestartist.ytpa.enums.Orientation;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    private final PlayVideoCallback playVideoCallback;
    YouTubePlayer.PlayerStyle playerStyle;
    Orientation orientation;
    boolean showAudioUi;
    boolean showFadeAnim;
    private final Context context;
    private ArrayList<ContentList> videoList;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        @BindView(R.id.iv_thumbnail) ImageView ivThumbnail;
//        @BindView(R.id.tv_video_title) TextView tvVideoTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

    public VideoListAdapter(Context context, ArrayList<ContentList> videoList, PlayVideoCallback playVideoCallback){
        this.context = context;
        this.videoList = videoList;
        this.playVideoCallback = playVideoCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_view,
                        parent,
                        false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ContentList contentItem = videoList.get(position);

//        holder.tvVideoTitle.setText(contentItem.getTitle());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoListAdapter.this.playVideoCallback.playVideo(holder.getAdapterPosition());
            }
        });


        RemoteAccessor remoteAccessor = new RemoteAccessor();

        remoteAccessor.getVideoMetadata(contentItem.getVideoId(), new Callback<YoutubeMetadata>() {

            @Override
            public void success(YoutubeMetadata youtubeMetadata, Response response) {
                String thumbnailUrl = null;
                if (youtubeMetadata.getVideoMetadata().size() > 0) {
                    thumbnailUrl = youtubeMetadata.getVideoMetadata().get(0).getSnippet().getThumbnails().getDefault().getUrl();
                }

                if (thumbnailUrl != null) {
                    Picasso.with(context)
                            .load(thumbnailUrl)
                            .fit()
                            .into(holder.ivThumbnail);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }, MainActivity.API_KEY);
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }
}
