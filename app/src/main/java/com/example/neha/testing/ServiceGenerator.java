package com.example.neha.testing;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class ServiceGenerator {
    private static String token = "";
    private static String YOUTUBE_URL = "https://www.googleapis.com/youtube/";

    // No need to instantiate this class.
    private ServiceGenerator() {
    }

    public static void setToken(String token) {
        ServiceGenerator.token = token;
    }

    public static <S> S createService(Class<S> serviceClass, String baseUrl) {
        // set endpoint url and use OkHTTP as HTTP client
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(baseUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }

    public static <S> S createYoutubeService(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(YOUTUBE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }
}
