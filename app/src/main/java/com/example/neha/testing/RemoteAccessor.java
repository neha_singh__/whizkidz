package com.example.neha.testing;

import com.example.neha.testing.api.YoutubeMetadata;

import java.util.ArrayList;

import retrofit.Callback;

public class RemoteAccessor {
    public static String BASE_URL = "http://54.179.149.254/api";


    protected MyAppService service = ServiceGenerator.createService(MyAppService.class,
            BASE_URL);

    protected YoutubeService youtubeService = ServiceGenerator.createYoutubeService(YoutubeService.class);


    void ContentList(Callback<ArrayList<ContentList>> callback){
        service.ContentList(callback);
    }

    public void getVideoMetadata(String videoId, Callback<YoutubeMetadata> callback, String API_KEY) {
        youtubeService.getYoutubeMetadata(videoId, API_KEY, "snippet,statistics", callback);

    }
}
