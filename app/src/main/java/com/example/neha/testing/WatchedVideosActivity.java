package com.example.neha.testing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.neha.testing.database.WatchedVideos;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WatchedVideosActivity extends AppCompatActivity {
    @BindView(R.id.lv_watched) ListView lvWatchedVideos;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.watched_videos_activity);
        ButterKnife.bind(this);

        List<WatchedVideos> watchedVideos = WatchedVideos.getAll();

        ArrayAdapter adapter = new CustomCardAdaptor(this, R.layout.card_view_name, watchedVideos);

        lvWatchedVideos.setAdapter(adapter);
    }
}
