package com.example.neha.testing.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;


@Table(name = "WatchedVideos")
public class WatchedVideos extends Model {
    @Column(name = "Name")
    public String name;

    public WatchedVideos(String name) {
        super();
        this.name = name;
    }

    public WatchedVideos() {
        super();
    }

    public static List<WatchedVideos> getAll() {
        return new Select()
                .from(WatchedVideos.class)
                .execute();
    }

    public static void insert(String title) {
        WatchedVideos watchedVideos = new WatchedVideos(title);
        watchedVideos.save();
    }
}
