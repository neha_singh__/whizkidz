package com.example.neha.testing.api;

import com.google.gson.annotations.SerializedName;

public class YoutubeThumbnail {
    @SerializedName("default")private Thumbnail _default;

    public Thumbnail getDefault() {
        return _default;
    }

    public void setDefault(Thumbnail _default) {
        this._default = _default;
    }
}
