package com.example.neha.testing.api;

public class YoutubeSnippet {

    private YoutubeThumbnail thumbnails;

    public YoutubeThumbnail getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(YoutubeThumbnail thumbnails) {
        this.thumbnails = thumbnails;
    }
}
