package com.example.neha.testing;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.neha.testing.database.WatchedVideos;

import java.util.List;


public class CustomCardAdaptor extends ArrayAdapter {
    private List<WatchedVideos> watched;

    public CustomCardAdaptor(Context context, int resource, List<WatchedVideos> watched) {
        super(context, resource, watched);
        this.watched = watched;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.card_view_name, null);
        }

        ((TextView) v.findViewById(R.id.info_text)).setText(watched.get(position).name);

        return v;
    }
}