package com.example.neha.testing;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;

public interface MyAppService {

    @GET("/v1/content/listing/41/")
    void ContentList(Callback<ArrayList<ContentList>> callback);
}
