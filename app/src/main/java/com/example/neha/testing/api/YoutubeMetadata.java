package com.example.neha.testing.api;

import java.util.ArrayList;

public class YoutubeMetadata {
    private ArrayList<VideoMetadata> items;

    public ArrayList<VideoMetadata> getVideoMetadata() {
        return items;
    }

    public void setVideoMetadata(ArrayList<VideoMetadata> items) {
        this.items = items;
    }
}
