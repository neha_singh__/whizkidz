package com.example.neha.testing;

public class ContentList {

    private int id;
    private String title;
    private String url;
    private String desc;
    private int status;
    private String category;
    private String theme;
    private String sub_theme;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getSub_theme() {
        return sub_theme;
    }

    public void setSub_theme(String sub_theme) {
        this.sub_theme = sub_theme;
    }

    public String getVideoId() {
        String[] videoUrlSplit = url.split("=");
        String videoId = null;

        if (videoUrlSplit != null && videoUrlSplit.length > 1) {
            videoId = videoUrlSplit[1];
        }

        return videoId;
    }
}
